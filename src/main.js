import Vue from 'vue'
import App from './App.vue'
import store from './store'
import './assets/style.scss'
import iconSet from 'quasar/icon-set/material-icons'
import Quasar, {
  QBtn,
  QTable,
  QIcon,
  QTr,
  QTd,
  QTh
} from 'quasar'

Vue.config.productionTip = false
Vue.use(Quasar, {
  components: {
    QBtn,
    QTable,
    QIcon,
    QTr,
    QTd,
    QTh,
  },
  iconSet
})
new Vue({
  render: h => h(App),
  store
}).$mount('#app')
